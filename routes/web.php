<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();

Route::group(['middleware'=>['guest']],function(){
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('login.form');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::get('/login','Auth\LoginController@showLoginForm' )->name('login');
});
Route::group(['middleware' => ['auth']], function () {
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('/newemployed/{edit?}', 'EmployedController@registerEmployed')->name('new.employed');
    Route::post('/employed/enabled', 'EmployedController@enabledAccessEmployed')->name('employed.enabled');
    Route::post('/employed/disabled', 'EmployedController@disabledAccessEmployed')->name('employed.disabled');
    Route::post('/employed/deleted', 'EmployedController@deleteEmployed')->name('employed.deleted');
    Route::get('/employed/getEmployees', 'EmployedController@getEmployees')->name('employees.get');
    Route::get('/employed/import', 'EmployedController@indexImport')->name('employees.import');
    Route::post('/employed/import', 'EmployedController@ImportEmployeds')->name('employees.import');
    Route::get('/employed/formaccess', 'EmployedController@indexFormAccess')->name('employees.access');
    Route::post('/employed/formaccess/{exit?}', 'EmployedController@accessEmployed')->name('employees.access');
    Route::get('/employed/history/{id}', 'EmployedController@download')->name('employees.history');

    //Users
    Route::get('/users/index', 'UserController@index')->name('user.index');
    Route::post('/users/created', 'UserController@createUser')->name('user.created');
    //Route::post('/employed/import', 'EmployedController@ImportEmployeds')->name('employees.import');
});
Route::get('/home', 'EmployedController@getEmployees')->name('home');
