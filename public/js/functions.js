
$(document).ready(function(){
    
    $('#formSubmit').click(function(e){
        e.preventDefault();
        var buton = $('#formSubmit');
        var edit = buton.data('edit')
        let urlAction = edit ? '/newemployed/true' : '/newemployed';
        $.ajaxSetup({
            headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        });
        $.ajax({
            url: urlAction,
            method: 'post',
            data: {
                employed_id: $('#employed_id').val(),
                firs_name: $('#firs_name').val(),
                last_name: $('#last_name').val(),
                middle_name: $('#middle_name').val(),
                departament_id: $('#departament_id').val(),
            },
            success: function(result){
                if(result.errors)
                {
                    console.log(result.errors)
                    $('.alert-danger').html('');

                    $.each(result.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
                else
                {
                    alert(result.success);
                    window.location.reload();
                }
            }
        });
    });
    $('.editUser').click( function() {
        event.preventDefault();
        var data = $(this).data('id');
        var buton = $('#formSubmit');
        var edit = buton.attr('data-edit', "true");
        console.log(data['employed_id']);
        $('.modal-title').html(" !! Edit Employed");
        $('#formSubmit').html("Edit");
        $('#employed_id').val(data['employed_id']);
        $('#firs_name').val(data['firs_name']);
        $('#last_name').val(data['last_name']);
        $('#middle_name').val(data['middle_name']);
        $('#departament_id').val(data['departament_id']);
    });
    
    $('.close','#closeModal').click( function() {
        event.preventDefault();
        var data = $(this).data('id');
        var buton = $('#formSubmit');
        var edit = buton.attr('data-edit', "false");
        console.log(buton.data('edit'))
        
        $('.modal-title').html("Register new employed !!");
        $('#formSubmit').html("Save");
        $('#employed_id').val('');
        $('#firs_name').val('');
        $('#last_name').val('');
        $('#middle_name').val('');
        $('#departament_id').val('');
    })

    $('.activeEmployed').click( function(e) {
        var idEmployed = $(this).data('id');
        e.preventDefault();
        $.ajaxSetup({
          headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        });
        $.ajax({
            url:'/employed/enabled',
            method: 'post',
            data: {
              employed_id : idEmployed,
            },
            success: function(result){
                if(result.errors)
                {
                  alert("an error ocurred.")
                  console.log(result.errors);
                }
                else{
                  alert(result.success)
                  window.location.reload();
                }
            }
        });
    });

    $('.inactiveEmployed').click( function(e) {
    var idEmployed = $(this).data('id');
    e.preventDefault();
    $.ajaxSetup({
        headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    });
    $.ajax({
        url: '/employed/disabled',
        method: 'post',
        data: {
            employed_id : idEmployed,
        },
        success: function(result){
            if(result.errors)
            {
                alert("an error ocurred.")
                console.log(result.errors);
            }
            else{
                alert(result.success)
                window.location.reload();
            }
        }
    });
    });

    $('.deletedEmployed').click( function(e) {
    var idEmployed = $(this).data('id');
    e.preventDefault();
    $.ajaxSetup({
        headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    });
    $.ajax({
        url: '/employed/deleted',
        method: 'post',
        data: {
            employed_id : idEmployed,
        },
        success: function(result){
            if(result.errors)
            {
                alert("an error ocurred.")
                console.log(result.errors);
            }
            else{
                alert(result.success)
                window.location.reload();
            }
        }
    });
    });

    $('#btnAccess').click( function(e) {
        e.preventDefault();
        var idEmployed =  $('#employed_id').val();
        $.ajaxSetup({
            headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        });
        $.ajax({
            url: '/employed/formaccess',
            method: 'post',
            data: {
                employed_id : idEmployed,
            },
            success: function(result){
                if(result.errors)
                {
                    console.log(result.errors)
                    $('.alert-danger').html('');

                    $.each(result.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
                else
                {
                    let access  = result.accessed;
                    if(access){
                        var result = confirm("The employee : "+idEmployed+" has already logged into the system, do you want to check out?");
                        if (result == true) {
                            $.ajaxSetup({
                                headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            });
                            $.ajax({
                                url: '/employed/formaccess/'+true,
                                method: 'post',
                                data: {
                                    employed_id : idEmployed,
                                },
                                success: function(result){
                                    if(result.errors)
                                    {
                                        console.log(result.errors)
                                        $('.alert-danger').html('');
                    
                                        $.each(result.errors, function(key, value){
                                            $('.alert-danger').show();
                                            $('.alert-danger').append('<li>'+value+'</li>');
                                        });
                                    }
                                    else
                                    {
                                        alert(result.success);
                                        window.location.reload();
                                    }
                                }
                            });
                        } else {
                            alert("Employee departure canceled");
                        }
                    }
                    else{
                        alert(result.success);
                        window.location.reload();
                    }
                    
                }
            }
        });
    });

    $('.historyEmployed').click( function(e) {
        var idEmployed = $(this).data('id');
        console.log(idEmployed);
        e.preventDefault();
        $.ajaxSetup({
            headers:  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        });
        $.ajax({
            url: '/employed/history',
            method: 'post',
            data: {
                employed_id : idEmployed,
            },
            success: function(result){
                if(result.errors)
                {
                    console.log(result.errors);
                }
                else{
                    alert(result.success)
                    //window.location.reload();
                }
            }
        });
    });

    $('.filterData').click( function(e) {
        //Session["rangedate"] = $('#rangedate').val();
    });

});