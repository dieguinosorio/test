@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <button type="button" style="margin-left:92%;" data-toggle="modal" data-target="#exampleModal" class="btn btn-secondary">
        <i class="icon-plus"></i>&nbsp;New Employed
    </button>
</ol>
@include('modals.modal')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <i class="fa fa-align-justify"></i> Employeds
        </div>
        <div class="card-body">
            <form role="form" method="GET" action="{{route('employees.get')}}">
                @csrf
                <div class="card card-info">
                    <div class="card-header bg-head ">
                        <h3 class="card-title">Search criteria</h3>
                    </div>
                        <div class="card-info">
                            <div class="card-body">
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Id Employed</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control"  id="id_employed" value="{{old('id_employed')}}" name="id_employed" />
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Departament</label>
                                            <div class="col-md-9">
                                            <select class="form-control" id="id_departament" name="id_departament">
                                            <option value="0">Select</option>
                                                @foreach($departments as $department)
                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Range Date Access</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control float-right"   name="rangedate" id="rangedate"  pleaceholder="Select dates">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                            <div class="col-md4 offset-4">
                                <input type="submit" class="filterData btn btn-flat btn-info" text="Search">
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </form>
            @if(session('message_success'))
                <div id="msgsuccess" class="alert alert-success alert-dismissible fade show">{{session('message_success')}}
                    @if(session('dataNoinsert'))
                    <?php $Cont =0; ?>
                        @foreach(session('dataNoinsert') as $data)
                            <li>{{$data[0]}}</li>
                        @endforeach
                    @endif
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif(session('message_error'))
                <div id="msgsuccess" class="alert alert-danger alert-dismissible fade show">{{session('message_error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <table class="table table-bordered table-striped table-sm">
                <thead>
                    <tr>
                        <th>Employed ID</th>
                        <th>Departament</th>
                        <th>Last Name</th>
                        <th>Middle Name</th>
                        <th>First Name</th>
                        <th>Last Access</th>
                        <th>Total Access</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                @if(isset($employees) && count($employees)>0)
                <tbody>
                    @foreach($employees  as $employed)
                    <tr>
                        <td>{{$employed->employed_id}}</td>
                        <td>{{$employed->departament->name}}</td>
                        <td>{{$employed->last_name}}</td>
                        <td>{{$employed->middle_name}}</td>
                        <td>{{$employed->firs_name}}</td>
                        <td>{{$employed->LastAccess}}</td>
                        <td>{{$employed->CountAccess}}</td>
                        <td>
                            @if(!$employed->inactive)
                                <button  class="editUser btn btn-primary btn-sm" data-id="{{$employed}}" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-edit"></i> Edit</button>
                                @if($employed->inactive_access)
                                <button  class="activeEmployed btn btn-success btn-sm" data-id="{{$employed->employed_id}}"><i class="fas fa-check"></i> Active Access</button>
                                @else
                                <button  class="inactiveEmployed btn btn-warning btn-sm" data-id="{{$employed->employed_id}}"><i class="fas fa-minus-circle"></i> Inactive Access</button>
                                @endif
                                <button  class="deletedEmployed btn btn-danger btn-sm" data-id="{{$employed->employed_id}}"><i class="fas fa-trash"></i> Delete</button>
                                <a class="btn btn-info btn-sm" href="{{route('employees.history',$employed->employed_id)}}"><i class="fas fa-folder"></i> History</a>
                                <!--<button  class="historyEmployed btn btn-info btn-sm" data-id="{{$employed->employed_id}}" ><i class="fas fa-folder"></i> History</button>-->
                            @else
                                <spam class="badge badge-danger">Is deleted</spam>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                @else
                <tbody>
                    <tr>
                        <td colspan="8" >Not found register</td>
                    </tr>
                </tbody>
                @endif
            </table>
            <div class="clearfix">{{ $employees->links() }}</div>
        </div>
    </div>
    
</div>
@endsection
