@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
</ol>
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
        <form action="{{ route('user.created') }}" method="POST">
                @csrf
                <div class="card card-info">
                    <div class="card-header bg-head ">
                        <h3 class="card-title">Register Users</h3>
                    </div>
                    <div class="alert alert-danger" style="display:none"></div>
                    <div class="card-info">
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                        <div class="col-md4 offset-4">
                            <input type="submit" class="btn btn-flat btn-info" value="Register">
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-striped table-sm">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>email</th>
                        <th>role</th>
                    </tr>
                </thead>
                @if(isset($users) && count($users)>0)
                <tbody>
                    @foreach($users  as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->description}}</td>
                    </tr>
                    @endforeach
                </tbody>
                @else
                <tbody>
                    <tr>
                        <td colspan="3" >Not found register</td>
                    </tr>
                </tbody>
                @endif
            </table>
        </div>
    </div>
    
</div>
@endsection
