<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
body{
    font
}
#tablehead {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tablehead td,
th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 1px;
}

#tablehead tr:nth-child(even) {
    background-color: #dddddd;
}

#tableinfo {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tableinfo td,
#tableinfo th {
    border: 1px solid #ddd;
    padding: 3px;
}

#tableinfo tr:nth-child(even) {
    background-color: #f2f2f2;
}

#tableinfo tr:hover {
    background-color: #ddd;
}

#tableinfo th {
    padding-top: 8px;
    padding-bottom: 8px;
    text-align: left;
    background-color: rgb(205, 212, 205);
    color: rgb(28, 29, 34);
}
</style>
<body>
<div class="wrapper" id="app">
  <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
           @yield('content')
        </div>
    </section>
  </div>
</div>
</body>
</html>
