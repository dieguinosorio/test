 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <ul class="nav navbar-nav ml-auto">
          <div class="menu-rigth" v-if="{{\Auth::check()}}">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <span class="d-md-down-none" ><strong>Welcome</strong> {{Auth::user()->name}} </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                      <div class="dropdown-header text-center">
                          <strong>Cuenta</strong>
                      </div>
                      <a class="dropdown-item" href="{{ route('logout') }}" 
                          onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="fa fa-lock"></i> Cerrar sesión</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </div>
              </li>
          </div>
      </ul>
    </ul>
  </nav>
  <!-- /.navbar -->