@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
</ol>
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
        <div class="form-group"  onload="new_clock()">
            <label id="fecha_registro" name="fecha_registro"></label>
        </div>
        <form>
                @csrf
                <div class="card card-info">
                    <div class="card-header bg-head ">
                        <h3 class="card-title">Access Employed</h3>
                    </div>
                    <div class="alert alert-danger" style="display:none"></div>
                        <div class="card-info">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Id Employed</label>
                                        <div class="col-md-9">
                                            <input type="number"  name="employed_id" id="employed_id" class="form-control" pleaceholder="Insert id employed">
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                            <div class="col-md4 offset-4">
                                <button id="btnAccess" class="btn btn-flat btn-info">
                                    Access
                                </button>
                            </div>
                            </div>
                        </div>
                </div>
            </form>
        </div>
    </div>
    
</div>
@endsection
