<!-- Modal -->
<div class="modal fade" class="exampleModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Register new employed !!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" style="display:none"></div>
                <form class="image-upload" method="post" action="{{ route('new.employed') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Id</label>
                        <input type="text" name="employed_id" id="employed_id" class="form-control"/>
                    </div>  

                    <div class="form-group">
                        <label>Firs Name</label>
                        <input type="text" name="firs_name" id="firs_name" class="form-control"/>
                    </div>  

                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" id="last_name" class="form-control"/>
                    </div>  

                    <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" name="middle_name" id="middle_name" class="form-control"/>
                    </div>  

                    <div class="form-group">
                        <label>Departament</label>
                        <select class="form-control" id="departament_id" name="departament_id">
                           <option value="0">Select</option>
                            @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>  

                </form>
            </div>
            <div class="modal-footer">
                <button type="button"  id="closeModal" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="formSubmit" data-edit="false">Save</button>
            </div>
        </div>
    </div>
</div>
