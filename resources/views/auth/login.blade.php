@extends('auth.contenido')

@section('login')
  <div class="login-form">
      <div class="col-md-8">
      <div class="card-group mb-0">
          <div class="card p-4">
          <form class="form-horizontal was-validated" method="POST" action="{{ route('login')}}">
              {{ csrf_field() }}
                <div class="card-body">
                <h1>Login</h1>
                <div class="form-group mb-3{{$errors->has('user' ? 'is-invalid' : '')}}">
                  <span class="input-group-addon"><i class="icon-user"></i></span>
                  <input type="text" value="{{old('user')}}" name="user" id="user" class="form-control" placeholder="user">
                  {!!$errors->first('user','<span class="invalid-feedback">:message</span>')!!}
                </div>
                <div class="form-group mb-4{{$errors->has('password' ? 'is-invalid' : '')}}">
                  <span class="input-group-addon"><i class="icon-lock"></i></span>
                  <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                  {!!$errors->first('password','<span class="invalid-feedback">:message</span>')!!}
                </div>
                <div class="row">
                  <div class="col-6">
                    <button type="submit" class="btn btn-primary px-4">Login</button>
                  </div>
                </div>
              </div>
          </form>
          </div>
          <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
          <div class="card-body text-center">
              <div>
              <h2>Admin Access Control</h2>
              <p>Developed by Diego O. All rights reserved.</p>
              </div>
          </div>
          </div>
      </div>
    </div>
</div>

@endsection
