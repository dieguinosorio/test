@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
</ol>
@include('modals.modal')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
        <form action="{{ route('employees.import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card card-info">
                    <div class="card-header bg-head ">
                        <h3 class="card-title">Import Employeds</h3>
                        
                    </div>
                        <div class="card-info">
                            <div class="card-body">
                                @if(session('errors'))
                                <div id="msgerror" class="alert alert-danger alert-dismissible fade show">
                                    @foreach(session('errors') as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Departament</label>
                                        <div class="col-md-9">
                                        <select class="form-control" id="id_departament" name="id_departament">
                                        <option value="0">Select</option>
                                            @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                        <label class="col-md-3 col-form-label">File CSV</label>
                                        <div class="col-md-9">
                                            <div class="custom-file">
                                                <input type="file"  name="file" class="form-control">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                            <div class="col-md4 offset-4">
                                <input type="submit" class="btn btn-flat btn-info" text="Import">
                            </div>
                            </div>
                        </div>
                </div>
            </form>
        </div>
    </div>
    
</div>
@endsection
