@extends('layouts.pdf')

@section('content')
<header>
    <div id="logo">
    </div>
    <div id="datos">
        <p id="encabezado">
            <b>System logins {{$employed->firs_name . " ". $employed->last_name }} ID : {{$employed->employed_id}}</b>
        </p>
    </div>
</header>
<hr>
<body>

    <table id="tableinfo">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Type</th>
            </tr>
        </thead>
        <tbody>
        @if(is_countable($dataAccess) && count($dataAccess)>0)
            @foreach($dataAccess as $access)
                <tr>
                    <td>{{$access->id}}</td>
                    <td>{{$access->date_access}}</td>
                    <td>{{$access->access}}</td>
                </tr>
                
            @endforeach
            </tbody>
        @else
            <tbody>
                <tr>
                    <td colspan="3">Not found registers</td>
                </tr>
            </tbody>     
        @endif                                               
    </table>
        
    <div class="izquierda">
        <p><strong>Total register: {{count($dataAccess)}}</strong></p>
    </div>    
</body>

@endsection