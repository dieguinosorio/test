<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employed', function (Blueprint $table) {
            $table->increments('id',100);
            $table->integer('employed_id')->length(11);
            $table->Integer('departament_id')->unsigned();
            $table->foreign('departament_id')->references('id')->on('departments');
            $table->string('last_name',255)->notnull();
            $table->string('middle_name',255)->notnull();
            $table->string('firs_name',255)->notnull();
            $table->tinyInteger('inactive_access')->length(1);
            $table->dateTime('inactive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employed');
    }
}
