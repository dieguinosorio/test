<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->role_id = 1 ;
        $user->email = 'admon@admin.com';
        $user->password = Hash::make('admin123.');
        $user->save();
    }
}
