<?php

use Illuminate\Database\Seeder;
use App\Model\Departments;
class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Departments();
        $role->name = 'administration';
        $role->description = 'administration room 911';
        $role->save();

        $role = new Departments();
        $role->name = 'logistics';
        $role->description = ' storage';
        $role->save();
    }
}
