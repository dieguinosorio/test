<?php
namespace App\Helpers;
  
use Illuminate\Support\Facades\DB;
use App\Model\Employed;
use App\Model\LogAccess;
use DateTime;

class Functions {
    public static function getEmployees($IdEmployed = null,$IdDep = null,$StarDate = null,$EndDate = null){
        try{
            $employees = Employed::select(
                'employed.*',
                DB::raw('(select log_access.date_access from log_access where log_access.employed_id = employed.employed_id and access <> 0 order by date_access desc limit 1) as LastAccess'),
                DB::raw('(select count(log_access.employed_id) from log_access where log_access.employed_id = employed.employed_id and access = 1 ) as CountAccess')
            );
            if($IdEmployed){
                $employees = $employees->where(DB::raw('CONCAT_WS(employed_id,last_name,middle_name,firs_name)'),'like' ,  "%".$IdEmployed."%" );
            }
            if($IdDep <> 0){
                $employees = $employees->where('departament_id',$IdDep);
            }
            if($StarDate && $EndDate){
                $StarDate = date_format (new \DateTime($StarDate), 'Y-m-d');
                $EndDate = date_format (new \DateTime($EndDate), 'Y-m-d');
                $SubQuery = DB::select("select log_access.employed_id from log_access where  access = 1  and DATE_FORMAT(date_access,'%Y-%m-%d') between '".$StarDate."' and '".$EndDate."'");
                $SubQuery = (json_decode(json_encode($SubQuery), true));
                $employees = $employees->whereIn('employed.employed_id',$SubQuery);
            }
            $employees = $employees->orderBy('firs_name')->paginate(15);
            return[
                'employees'=>$employees,
                'status'=>200,
                'msg'=>'Successful consultation'
            ];
        }
        catch(Exception $e){
            return[
                'employees'=>null,
                'status'=>500,
                'msg'=>'Oops, an error occurred '.$e->getMessage()
            ];
        }
    }

    public static function getDepartments(){
        try{
            $departments = DB::select("select * from departments order by name");
            return[
                'departments'=>$departments,
                'status'=>200,
                'msg'=>'Successful consultation'
            ];
        }
        catch(Exception $e){
            return[
                'departments'=>null,
                'status'=>500,
                'msg'=>'Oops, an error occurred '.$e->getMessage()
            ];
        }
    }

    public static function logAccess($IdEmployed,$OpAccess){
        try {
            $log = new logAccess();
            $log->employed_id = $IdEmployed;
            $log->access = $OpAccess;
            $log->date_access = date("Y-m-d H:i:s");
            $log->save();
            return true;
        } catch (Exception $th) {
            return false;
        }
    }

    public static function getLastAccess($IdEmployed){
        $lastAccess = DB::select("select * from log_access where  employed_id =".$IdEmployed."  order by date_access desc limit 1");
        return $lastAccess;
    }

    public static function getRegisterAccessEmployed($IdEmployed,$rangesDate=null){
        $StarDate = null;
        $EndDate = null;
        if($rangesDate){
            $StarDate = date_format (new \DateTime($rangesDate[0]), 'Y-m-d');
            $EndDate = date_format (new \DateTime($rangesDate[1]), 'Y-m-d');
        }
        $StrSql = " select id,(CASE
                        WHEN access = 0 THEN 'No registration income'
                        WHEN access = 1 THEN 'System Login'
                        ELSE 'Exit the system'
                    END) as access ,date_access from log_access where employed_id = ".$IdEmployed;
        if($StarDate && $EndDate){
            $StrSql.=" and DATE_FORMAT(date_access,'%Y-%m-%d') between '".$StarDate."' and '".$EndDate."'";
        }
        $StrSql.=" ORDER BY date_access ASC";
        $Registers = DB::select($StrSql);
        return $Registers;
    }

    public static function getDateFilter($url){
        try{
            if($url && strpos($url,'rangedate') !== false){
                $UrlAnt = url()->previous();
                $UrlAnt = explode('rangedate=',urldecode($UrlAnt));
                if(is_countable($UrlAnt) && count($UrlAnt)>0){
                    $dates = explode('-',$UrlAnt[1]);
                    return $dates;
                }
                return null;
            }
            return null;
        }
        catch(ErrorException  $e){
            return  null;
        }
    }
}