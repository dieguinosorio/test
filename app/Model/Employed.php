<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employed extends Model
{
    protected $table="employed";

    protected $fillable = [
        'employed_id',
        'firs_name',
        'last_name',
        'middle_name',
        'departament_id',
        'inactive_access',
    ];

    public function departament(){
        return $this->hasOne('App\Model\Departments','id','departament_id');
    }
}
