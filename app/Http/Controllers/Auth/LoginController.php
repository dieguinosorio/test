<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
       return view('auth.login'); 
    }

    public function login(Request $request)
    {
        $this->validatelogin($request);

        if(Auth::attempt(['name'=>$request->user,'password'=>$request->password])){
            return redirect()->route('home');
        }

        return back()->withErrors(['user'=>trans('auth.failed')])->withInput(['user'=>$request->user]);
    }

    protected function validatelogin($request)
    {
        $this->validate($request,[
            'user'=>'required|string',
            'password'=>'required|string',
        ]);
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
