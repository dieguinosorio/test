<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $users = User::with('role')->orderBy('name')->get();
        return view('users/form',array('users'=>$users));
    }

    public function createUser(Request $request){
        $input = $request->all();
        $validator =  \Validator::make($input, [
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        if($validator->fails()){    
            return redirect()->route('user.index')->withErrors($validator);
        }
        User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'role_id' => 1,
            'password' => \Hash::make($input['password']),
        ]);
        return redirect()->route('user.index')->with([]);
    }
}
