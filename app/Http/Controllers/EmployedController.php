<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Input;
use App\Model\Employed;
use SweetAlert;
use Barryvdh\DomPDF\Facade as PDF;
class EmployedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getEmployees(Request $request){
        $Employed = $request->input('id_employed') ?  $request->input('id_employed') : null;
        $IdDepartament = $request->input('id_departament') != 0 ? $request->input('id_departament') : null ;
        $Date = $request->input('rangedate') ? $request->input('rangedate') :null;
        $StarDate = null;
        $EndDate = null;
        if($Date){
            $Date = $Date ? explode('-' , $Date) : null;
            $StarDate = $Date[0];
            $EndDate = $Date[1];
        }
        $employees = \Functions::getEmployees($Employed,$IdDepartament,$StarDate,$EndDate);
        $departments = \Functions::getDepartments();
        return view('accesscontrol.index', array("employees" => $employees['employees'],'departments'=>$departments['departments']))->withInput($request->only('rangedate'));
    }

    public function registerEmployed(Request $request){
        try{
            $input = $request->all();
            
            $ValidExist = Employed::where('employed_id', '=', $request['employed_id'])->first();
            $validator = \Validator::make($request->all(), [
                'employed_id'=> 'required',
                'firs_name'=> 'required',
                'last_name'=> 'required',
                'middle_name'=> 'required',
                'departament_id'=> 'required',
            ]);
            
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($ValidExist  && $ValidExist->inactive){
                Employed::where('employed_id', '=', $request['employed_id'])->update($input);
                $ValidExist->inactive = null;
                $ValidExist->save();
                return response()->json(['success'=>'Employed is successfully added']);
            }
            else{
                $edit = isset($request->edit) && $request->edit ? true :false;
                
                if($ValidExist  && !$edit){
                    return response()->json(['errors'=>['Ups, The employee already exists with this id.'.$request['employed_id']]]);
                }

                if($edit){
                    Employed::where('employed_id', '=', $request['employed_id'])->update($input);
                    return response()->json(['success'=>'Employed is successfully update']);
                }
                else{
                    Employed::create($input);
                    return response()->json(['success'=>'Employed is successfully added']);
                }
            }
        }
        catch(Exception $e){
            return response()->json(['errors'=>[$e->getMessage()]]);
        }
    }

    public function enabledAccessEmployed(Request $request){
        try{
            $employed = Employed::where('employed_id', '=', $request['employed_id'])->first();
            $employed->inactive_access = 0;
            $employed->save();
            return response()->json(['success'=>'This employed is enabled access']);
        }
        catch(Exception $e){
            return response()->json(['errors'=>[$e->getMessage()]]);
        }
    }

    public function disabledAccessEmployed(Request $request){
        try{
            $employed = Employed::where('employed_id', '=', $request['employed_id'])->first();
            $employed->inactive_access = 1;
            $employed->save();
            return response()->json(['success'=>'This employed is disabled access']);
        }
        catch(Exception $e){
            return response()->json(['errors'=>[$e->getMessage()]]);
        }
    }

    public function deleteEmployed(Request $request){
        try{
            $employed = Employed::where('employed_id', '=', $request['employed_id'])->first();
            $employed->inactive = date("Y-m-d H:i:s");
            $employed->save();
            return response()->json(['success'=>'This employed is deleted']);
        }
        catch(Exception $e){
            return response()->json(['errors'=>[$e->getMessage()]]);
        }
    }

    public function indexImport(){
        $departments = \Functions::getDepartments();
        return view('employed/import',array('departments'=>$departments['departments']));
    }

    public function ImportEmployeds(Request $request){
        $EmployedsCsv = Input::file('file');
        $DepartmentId = $request->input('id_departament');
        $validator = \Validator::make($request->all(), ['file' => 'required|mimes:csv,txt','id_departament'=> 'required|numeric|min:0|not_in:0']);
        if($validator->fails()){    
            return redirect()->route('employees.import')->with(['errors'=>$validator->errors()->all()]);
        }
        $fp = fopen($EmployedsCsv->getRealPath(), "r");
        $TotalInsert = 0;
        $TotalNoInsert = 0;
        $DataNoInsert = [];
        while (($data = fgetcsv($fp, 1000, ";")) !== FALSE){
            try{
                $ValidExist = Employed::where('employed_id', '=', $data[0])->first();
                if(!$ValidExist){
                    $NewEmployed = new Employed();
                    $NewEmployed->employed_id = $data[0];
                    $NewEmployed->departament_id = $DepartmentId;
                    $NewEmployed->last_name = $data[1];
                    $NewEmployed->middle_name = $data[2];
                    $NewEmployed->firs_name = $data[3];
                    $NewEmployed->save();
                    $TotalInsert++;
                }
                else{
                    $DataNoInsert[] = $data;
                    $TotalNoInsert++;
                }
            }
            catch(\Illuminate\Database\QueryException $ex){
                $DataNoInsert[] = $data;
                $TotalNoInsert++;
            }
            catch(Exception $e){
                $DataNoInsert[] = $data;
                $TotalNoInsert++;
            }
            catch(ErrorException $eex){
                $DataNoInsert[] = $data;
                $TotalNoInsert++;
            }
        }
        $departments = \Functions::getDepartments();
        return redirect("/home")->with(array('message_success'=> 'Employees have been imported, Total import :'.$TotalInsert." and not insert :".$TotalNoInsert,'dataNoinsert'=>$DataNoInsert));
    }

    public function indexFormAccess(){
        return view('formaccess/form');
    }

    public function accessEmployed(Request $request){
        try {
            
            $validator = \Validator::make($request->all(), [
                'employed_id'=> 'required',
            ]);
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            $IdEmployed = $request['employed_id'];
            $LastAccess = \Functions::getLastAccess($IdEmployed);
            $EmployedExist = Employed::where('employed_id', '=', $IdEmployed)->where('inactive_access',0)->whereNull('inactive')->first();
            if($request->exit){
                \Functions::logAccess($IdEmployed, 2);
                return response()->json(['success'=>'The employee has been successful '.$request['employed_id'],'accessed'=>null]);
            }
            else{
                if(($EmployedExist) && $LastAccess && $LastAccess[0]->access == 1){
                    return response()->json(['success'=>'The employee has already accessed the system'.$request['employed_id'],'accessed'=>true]);
                }
                else{
                    if($EmployedExist){
                        \Functions::logAccess($IdEmployed, 1);
                        return response()->json(['success'=>'Employed  access'.$request['employed_id'],'accessed'=>null]);
                    }else{
                        \Functions::logAccess($IdEmployed, 0);
                        return response()->json(['success'=>'Employee with id : '.$request['employed_id'].' does not exist in the system or is inactive.','accessed'=>null]);
                    }
                }
            }
            
            
        } catch (Exception $th) {

        }
    }

    public function download(Request $request){
        $RangeDate = \Functions::getDateFilter(url()->previous());
        $Employed = Employed::where('employed_id', '=', $request->id)->first();
        $DataAccess = \Functions::getRegisterAccessEmployed($request->id,$RangeDate);
        $pdf = PDF::loadView('employed/history', array('employed'=>$Employed,'dataAccess'=>$DataAccess));
        return $pdf->download('logs_'.$Employed->employed_id.'.pdf');
    }
}
