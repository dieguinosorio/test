const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.styles([
      'resources/vendor/fontawesome/css/all.min.css',
      'resources/vendor/css/adminlte.min.css',
      'resources/vendor/css/styles.css',
      'resources/vendor/css/daterangepicker.css',
      'resources/vendor/css/bootstrap-colorpicker.min.css',
   ], 'public/css/plantilla.css')
   .js('resources/js/app.js', 'public/js') //Este es el que inclue laravel que viene con boostap,Jquery,Vue
   .scripts([
      'resources/vendor/js/adminlte.js',
      'resources/vendor/js/adminlte.min.js',
      'resources/vendor/js/demo.js',
      'resources/vendor/js/jquery.min.js',
      'resources/vendor/js/moment.min.js',
      'resources/vendor/js/daterangepicker.js',
      'resources/vendor/js/bootstrap-colorpicker.min.js',
   ], 'public/js/plantilla.js')
.copy('resources/vendor/fontawesome/webfonts', 'public/webfonts')
.copy('resources/vendor/img', 'public/img');